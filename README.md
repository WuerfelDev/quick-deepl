# Quick DeepL

A browser extension that implements DeepL in a pop up window for simple access


## Todo

- cache last translation (storage vs background script?)
- settings on options page to use own api key
- adapt to light/dark mode
- advanced feature: get selected text on page
