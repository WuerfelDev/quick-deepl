// document.getElementById('translation').innerHTML = chrome.i18n.getUILanguage();


async function useAPI(path,data){
    url = "https://api-free.deepl.com/v2/"+path;
    url += "?auth_key="+auth_key;
    for (k in data) url += "&"+k+"="+data[k];
    const response = await fetch(url);
    if(response.ok){
        return response.json();
    }else{
        showError(response.status,response);
        return null;
    }
}


chrome.storage.local.get(['languages'], function(result) {
    if(result.languages == undefined){
        useAPI('languages',{'type':'target'}).then(r => {
            if(r){
                chrome.storage.local.set({'languages': r});
                setLanguages(r);
            }
        });
    }else{
        setLanguages(result.languages);
    }
});

function setLanguages(langs){
    console.log(langs);
    list = document.getElementById('languages');
    for (const lang of langs){
        opt = document.createElement('option');
        opt.value = lang['language'];
        opt.textContent = lang['name'];
        list.appendChild(opt);
    }
    chrome.storage.local.get(['mylang'], r => {
        document.getElementById('languages').value = r.mylang!=undefined?r.mylang:"EN-US";
    });
}

function showError(status,log=''){
    console.log(log);
    elem = document.getElementById('error');
    elem.setAttribute("style", "display:block;");
    text = "";
    switch(status){
        case 400: text = "Bad request";break;
        case 403: text = "Authorization failed";break;
        case 404: text = "Resource could not be found";break;
        case 413: text = "Request size exceeds the limit";break;
        case 414: text = "Request is too long";break;
        case 429: text = "Too many requests. Please wait and resend your request";break;
        case 456: text = "API quota exceeded<br><a href='https://www.deepl.com/translator#en/en/"+ encodeURIComponent(document.getElementById('input').value) +"'>Open DeepL in a tab</a>";break; // Might not work since we need to specify the source language
        case 503: text = "Resource currently unavailable. Try again later";break;
        case 529: text = "Too many requests. Please wait and resend your request";break;
        default: text = "Unknown error";break;
    }
    elem.textContent = "Error! HTTP code "+status+": "+text;
}

//https://stackoverflow.com/a/25621277
const tx = document.getElementById('input');
tx.setAttribute("style", "height:" + (tx.scrollHeight) + "px;overflow-y:hidden;");
tx.addEventListener("input", OnInput, false);
function OnInput() {
    this.style.height = "auto";
    this.style.height = (this.scrollHeight) + "px";
}





const translate = document.getElementById('translate');
translate.addEventListener("click", onTranslate, false);
function onTranslate(){
    lang = document.getElementById('languages').value;
    chrome.storage.local.set({'mylang': lang});
    text = document.getElementById('input').value;
    if(text.trim()!=''){
        useAPI('translate',{'text':encodeURIComponent(text),'target_lang':lang}).then(r => {
            if(r){
                document.getElementById('translation').textContent = r['translations'][0]['text'];
            }
        });
    }
}
